package com.example.interviewapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.interviewapp.databinding.ItemComponentOneBinding
import com.example.interviewapp.model.SAT
import com.example.interviewapp.model.School

class SATAdapter(var list: SAT, listener: SATAdapter.SATClickListener
) : RecyclerView.Adapter<SATAdapter.ObjectViewHolder>() {
 interface SATClickListener{
        fun onClick(sat:SAT)
    }
    val listener= listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectViewHolder {

        return ItemComponentOneBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                .let {
                    ObjectViewHolder(it,listener)

                }


    }
   override fun getItemCount() = 1


    override fun onBindViewHolder(holder: ObjectViewHolder, position: Int) {
     val data = list
        holder.load(data)
        holder.itemView.setOnClickListener { listener.onClick(data) }


    }

   


    class ObjectViewHolder(
       private val binding: ItemComponentOneBinding, listener: SATClickListener
    ) : RecyclerView.ViewHolder(binding.root) {
val listener= listener
        fun formulateText(data: SAT):String{
            return "name:"+data.school_name+"- writing:"+data.sat_writing_avg_score+"reading:"+data.sat_critical_reading_avg_score+"math:"+data.sat_math_avg_score
        }
        fun load(data: SAT) = with(binding) {
            tvText.text = formulateText(data)
            binding.root.setOnClickListener {
                listener.onClick(data)
            }
        }
    }






}


package com.example.interviewapp.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.interviewapp.R
import com.example.interviewapp.databinding.ItemComponentOneBinding
import com.example.interviewapp.model.School

class SchoolAdapter( var list: List<School>, listener: SchoolAdapter.SchoolClickListener
) : RecyclerView.Adapter<SchoolAdapter.ObjectViewHolder>() {
 interface SchoolClickListener{
        fun onClick(school:School)
    }
    val listener= listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ObjectViewHolder {

        return ItemComponentOneBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                .let {
                    ObjectViewHolder(it,listener)

                }


    }
   override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: ObjectViewHolder, position: Int) {
     val data = list[position]
        holder.load(data)
        holder.itemView.setOnClickListener { listener.onClick(data) }


    }

   


    class ObjectViewHolder(
       private val binding: ItemComponentOneBinding, listener: SchoolClickListener
    ) : RecyclerView.ViewHolder(binding.root) {
val listener= listener
        fun load(data: School) = with(binding) {
            tvText.text = data.school_name
            binding.root.setOnClickListener {
                listener.onClick(data)
            }
        }
    }




}


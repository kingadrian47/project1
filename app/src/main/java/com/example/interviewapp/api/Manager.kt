package com.example.interviewapp.api

import com.example.interviewapp.model.SAT
import com.example.interviewapp.model.School
import retrofit2.http.GET

class Manager {
    private val schoolService:SchoolService
    private val satService:SATService

    private val retrofit = ApiClient.providesRetrofit()
    private val retrofit2 = ApiClient.providesRetrofit2()

    init {
        schoolService = retrofit.create(SchoolService::class.java)
        satService=retrofit2.create(SATService::class.java)
    }

    suspend fun getSchools() = schoolService.getSchools()
    suspend fun getSAT()= satService.getSAT()

    interface SchoolService{

        @GET(".")
        suspend fun getSchools()
                : List<School>
    }
    interface SATService{

        @GET(".")
        suspend fun getSAT()
                : List<SAT>
    }
}
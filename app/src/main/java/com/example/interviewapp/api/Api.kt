package com.example.interviewapp.api

import com.example.interviewapp.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object ApiClient {
    private const val TIMEOUT = 60 * 1000.toLong()
   private const val SchoolUrl= "https://data.cityofnewyork.us/resource/s3k6-pzi2.json/"
    private const val SatUrl= "https://data.cityofnewyork.us/resource/f9bf-2cp4.json/"


    private fun providesOkHttpClient(): OkHttpClient {
        val httpLoggingInterceptor =
            HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT)
        val clientBuilder = OkHttpClient.Builder().connectTimeout(TIMEOUT, TimeUnit.SECONDS).readTimeout(
            TIMEOUT,
            TimeUnit.SECONDS
        ).writeTimeout(TIMEOUT, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            clientBuilder.addInterceptor(httpLoggingInterceptor)
        }

        return clientBuilder.build()
    }

    fun providesRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(SchoolUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .client(providesOkHttpClient())
        .build()
    fun providesRetrofit2(): Retrofit = Retrofit.Builder()
        .baseUrl(SatUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .client(providesOkHttpClient())
        .build()


}

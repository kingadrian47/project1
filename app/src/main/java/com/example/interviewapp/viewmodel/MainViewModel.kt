package com.example.interviewapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.interviewapp.api.Manager
import com.example.interviewapp.adapter.SchoolAdapter
import com.example.interviewapp.model.SAT
import com.example.interviewapp.model.School
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlin.collections.ArrayList

class MainViewModel : ViewModel() {
    var SATData: List<SAT> = ArrayList()
    var schoolData: List<School> = ArrayList()
     var satData: SAT? =null
     var school :School?=null
    private var _selectedSchool: MutableLiveData<School> = MutableLiveData()
    var selectedSchool: LiveData<School>? = _selectedSchool
    private val _schools = MutableLiveData<List<School>>()
    val schools: LiveData<List<School>>
        get() = _schools

    private val _sat = MutableLiveData<List<SAT>>()
    val sat: LiveData<List<SAT>>
        get() = _sat

    init {
        Log.d("View Model", "Init" )
        getSchools()
        getSAT()

    }

     fun getSchools(){
        viewModelScope.launch {
            _schools.value = Manager().getSchools().sortedBy {
                it.school_name
            }
            _schools.value?.apply {
                schoolData= this
            }


        }
    }
    fun getSAT(){
        viewModelScope.launch{
            _sat.value = Manager().getSAT()

            _sat.value?.apply{
                SATData=this
            }

        }
        Log.d("View Model", "sat is" +SATData?.toString())

    }

    fun getSATFromSchool(school: String):SAT{
        Log.d("View Model", "school is" +school)
        Log.d("View Model", "sat data is" +SATData.toString())


        var sat:SAT?= SATData.firstOrNull(){
           school.contains( it.school_name, true)
        }

        return sat?: SATData[0]
    }

     fun setSelectedSchool(school: School){
         Log.d("View Model", "selected is" +school.school_name)
         satData=getSATFromSchool(school.school_name)
         this.school=school
    }

}
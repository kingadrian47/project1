package com.example.interviewapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class School implements Parcelable {
    public String dbn;
    public String school_name;
    public String boro;
    public String overview_paragraph;
    public String school_10th_seats;
    public String academicopportunities1;
    public String academicopportunities2;
    public String ell_programs;
    public String neighborhood;
    public String building_code;
    public String location;
    public String phone_number;
    public String fax_number;
    public String school_email;
    public String website;
    public String subway;
    public String bus;
    public String grades2018;
    public String finalgrades;
    public String total_students;
    public String extracurricular_activities;
    public String school_sports;
    public String attendance_rate;
    public String pct_stu_enough_variety;
    public String pct_stu_safe;
    public String school_accessibility_description;
    public String directions1;
    public String requirement1_1;
    public String requirement2_1;
    public String requirement3_1;
    public String requirement4_1;
    public String requirement5_1;
    public String offer_rate1;
    public String program1;
    public String code1;
    public String interest1;
    public String method1;
    public String seats9ge1;
    public String grade9gefilledflag1;
    public String grade9geapplicants1;
    public String seats9swd1;
    public String grade9swdfilledflag1;
    public String grade9swdapplicants1;
    public String seats101;
    public String admissionspriority11;
    public String admissionspriority21;
    public String admissionspriority31;
    public String grade9geapplicantsperseat1;
    public String grade9swdapplicantsperseat1;
    public String primary_address_line_1;
    public String city;
    public String zip;
    public String state_code;
    public String latitude;
    public String longitude;
    public String community_board;
    public String council_district;
    public String census_tract;
    public String bin;
    public String bbl;
    public String nta;
    public String borough;

    protected School(Parcel in) {
        dbn = in.readString();
        school_name = in.readString();
        boro = in.readString();
        overview_paragraph = in.readString();
        school_10th_seats = in.readString();
        academicopportunities1 = in.readString();
        academicopportunities2 = in.readString();
        ell_programs = in.readString();
        neighborhood = in.readString();
        building_code = in.readString();
        location = in.readString();
        phone_number = in.readString();
        fax_number = in.readString();
        school_email = in.readString();
        website = in.readString();
        subway = in.readString();
        bus = in.readString();
        grades2018 = in.readString();
        finalgrades = in.readString();
        total_students = in.readString();
        extracurricular_activities = in.readString();
        school_sports = in.readString();
        attendance_rate = in.readString();
        pct_stu_enough_variety = in.readString();
        pct_stu_safe = in.readString();
        school_accessibility_description = in.readString();
        directions1 = in.readString();
        requirement1_1 = in.readString();
        requirement2_1 = in.readString();
        requirement3_1 = in.readString();
        requirement4_1 = in.readString();
        requirement5_1 = in.readString();
        offer_rate1 = in.readString();
        program1 = in.readString();
        code1 = in.readString();
        interest1 = in.readString();
        method1 = in.readString();
        seats9ge1 = in.readString();
        grade9gefilledflag1 = in.readString();
        grade9geapplicants1 = in.readString();
        seats9swd1 = in.readString();
        grade9swdfilledflag1 = in.readString();
        grade9swdapplicants1 = in.readString();
        seats101 = in.readString();
        admissionspriority11 = in.readString();
        admissionspriority21 = in.readString();
        admissionspriority31 = in.readString();
        grade9geapplicantsperseat1 = in.readString();
        grade9swdapplicantsperseat1 = in.readString();
        primary_address_line_1 = in.readString();
        city = in.readString();
        zip = in.readString();
        state_code = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        community_board = in.readString();
        council_district = in.readString();
        census_tract = in.readString();
        bin = in.readString();
        bbl = in.readString();
        nta = in.readString();
        borough = in.readString();
    }

    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dbn);
        dest.writeString(school_name);
        dest.writeString(boro);
        dest.writeString(overview_paragraph);
        dest.writeString(school_10th_seats);
        dest.writeString(academicopportunities1);
        dest.writeString(academicopportunities2);
        dest.writeString(ell_programs);
        dest.writeString(neighborhood);
        dest.writeString(building_code);
        dest.writeString(location);
        dest.writeString(phone_number);
        dest.writeString(fax_number);
        dest.writeString(school_email);
        dest.writeString(website);
        dest.writeString(subway);
        dest.writeString(bus);
        dest.writeString(grades2018);
        dest.writeString(finalgrades);
        dest.writeString(total_students);
        dest.writeString(extracurricular_activities);
        dest.writeString(school_sports);
        dest.writeString(attendance_rate);
        dest.writeString(pct_stu_enough_variety);
        dest.writeString(pct_stu_safe);
        dest.writeString(school_accessibility_description);
        dest.writeString(directions1);
        dest.writeString(requirement1_1);
        dest.writeString(requirement2_1);
        dest.writeString(requirement3_1);
        dest.writeString(requirement4_1);
        dest.writeString(requirement5_1);
        dest.writeString(offer_rate1);
        dest.writeString(program1);
        dest.writeString(code1);
        dest.writeString(interest1);
        dest.writeString(method1);
        dest.writeString(seats9ge1);
        dest.writeString(grade9gefilledflag1);
        dest.writeString(grade9geapplicants1);
        dest.writeString(seats9swd1);
        dest.writeString(grade9swdfilledflag1);
        dest.writeString(grade9swdapplicants1);
        dest.writeString(seats101);
        dest.writeString(admissionspriority11);
        dest.writeString(admissionspriority21);
        dest.writeString(admissionspriority31);
        dest.writeString(grade9geapplicantsperseat1);
        dest.writeString(grade9swdapplicantsperseat1);
        dest.writeString(primary_address_line_1);
        dest.writeString(city);
        dest.writeString(zip);
        dest.writeString(state_code);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(community_board);
        dest.writeString(council_district);
        dest.writeString(census_tract);
        dest.writeString(bin);
        dest.writeString(bbl);
        dest.writeString(nta);
        dest.writeString(borough);
    }
}
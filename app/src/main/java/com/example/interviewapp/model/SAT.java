package com.example.interviewapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SAT implements Parcelable {
    public String dbn;
    public String school_name;
    public String num_of_sat_test_takers;
    public String sat_critical_reading_avg_score;
    public String sat_math_avg_score;
    public String sat_writing_avg_score;


    protected SAT(Parcel in) {
        dbn = in.readString();
        school_name = in.readString();
        num_of_sat_test_takers = in.readString();
        sat_critical_reading_avg_score = in.readString();
        sat_math_avg_score = in.readString();
        sat_writing_avg_score = in.readString();
    }

    public static final Creator<SAT> CREATOR = new Creator<SAT>() {
        @Override
        public SAT createFromParcel(Parcel in) {
            return new SAT(in);
        }

        @Override
        public SAT[] newArray(int size) {
            return new SAT[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dbn);
        dest.writeString(school_name);
        dest.writeString(num_of_sat_test_takers);
        dest.writeString(sat_critical_reading_avg_score);
        dest.writeString(sat_math_avg_score);
        dest.writeString(sat_writing_avg_score);
    }
}

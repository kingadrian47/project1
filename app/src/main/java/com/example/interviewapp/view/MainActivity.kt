package com.example.interviewapp.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interviewapp.adapter.SchoolAdapter
import com.example.interviewapp.databinding.ActivityMainBinding
import com.example.interviewapp.model.School
import com.example.interviewapp.viewmodel.MainViewModel


class MainActivity : AppCompatActivity(), SchoolAdapter.SchoolClickListener {

    private lateinit var binding: ActivityMainBinding
    private val vm by viewModels<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initComponentList()
        initObservers()
    }

    private fun initComponentList() = with(binding.rvComponents) {
        layoutManager = LinearLayoutManager(context)
    }

    private fun initObservers() = with(vm) {
        vm.getSchools()
        schools.observe(this@MainActivity, Observer {
            val adapter = SchoolAdapter(it,this@MainActivity)
            binding.rvComponents.adapter = adapter
        })
    }

    override fun onClick(school: School) {
        vm.setSelectedSchool(school)
        val extras = Bundle()
        extras.putParcelable("sat", vm?.satData)
        Log.d("Main Activity", "score is is" +vm.satData?.sat_writing_avg_score)
        var myIntent = Intent(this, SATActivity::class.java).apply{
            putExtras(extras)
        }
        startActivity(myIntent)

    }



}
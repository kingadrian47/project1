package com.example.interviewapp.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interviewapp.adapter.SATAdapter
import com.example.interviewapp.databinding.SatActivityBinding
import com.example.interviewapp.model.SAT
import com.example.interviewapp.viewmodel.MainViewModel

class SATActivity : AppCompatActivity(), SATAdapter.SATClickListener {

    lateinit var binding: SatActivityBinding
    val vm by viewModels<MainViewModel>()
      var sat: SAT?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SatActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initComponentList()
        val bundle = intent?.extras
        if (bundle == null) {
            finish()
            return
        }
        sat =
            bundle!!.getParcelable("sat")
        sat?.let { initAdapter(it) }
        Log.d("SAT Activity", "school is" +sat)
    }


    private fun initComponentList() = with(binding.rvComponents) {
        layoutManager = LinearLayoutManager(context)
    }

    private fun initAdapter(sat: SAT) = with(vm) {
            val adapter :SATAdapter?= SATAdapter( sat,this@SATActivity)
            binding.rvComponents.adapter = adapter
    }

    override fun onClick(sat: SAT) {
        val myIntent = Intent(this, MainActivity::class.java)
        startActivity(myIntent)
        finish()
    }


}